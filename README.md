# cordova-plugin-jitsi-pd
Cordova plugin for Jitsi Meet React Native SDK. Works with both iOS and Android, and fixes the 64-bit binary dependency issue with Android found in previous versions of this plugin.

# Summary 
I created this repo as there were multiple versions of this plugin, one which worked for iOS and Android, but neither worked with both. This verison satisfies the 64bit requirement for Android and also works in iOS and is currently being used in production.

# Installation
`cordova plugin add https://abhi0476@bitbucket.org/prepdoor/jitsi_cordova_plugin.git`

# Usage
```
const roomId = 'custom-room-id';

jitsiplugin.loadURL('https://meet.jit.si/' + roomId, roomId, displayName, false, function (data) {
    if (data === "CONFERENCE_WILL_LEAVE") {
        jitsiplugin.destroy(function (data) {
            // call finished
        }, function (err) {
            console.log(err);
        });
    }
}, function (err) {
    console.log(err);
});
```

# Credits
* Big thanks to @Findmate for original plugin
