var exec = require('cordova/exec');

exports.loadURL = function(url, key, name, isInvisible, success, error) {
    exec(success, error, "JitsiPlugin", "loadURL", [url, key, name, !!isInvisible]);
};

exports.destroy = function(success, error) {
    exec(success, error, "JitsiPlugin", "destroy", []);
};
